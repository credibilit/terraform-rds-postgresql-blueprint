// VPC
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "rds-postgres-test"
  }
}

// Private subnets
resource "aws_subnet" "cache_a" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags {
    Name = "cache-a"
  }
}

resource "aws_subnet" "cache_b" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  tags {
    Name = "cache-b"
  }
}

// Variables
variable "account" {}

// RDS with HA
module "test" {
  source  = "../"
  account = "${var.account}"

  name = "test-postgres-acme"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  vpc_id = "${aws_vpc.default.id}"
  tags = {
    Foo = "bar"
  }
  password = "123mudar"
  username = "administrator"
  auto_minor_version_upgrade = true

  skip_final_snapshot = true
  multi_az = false
  
}

output "test" {
  value = "${module.test.db_instance}"
}

// External Parameter Group
resource "aws_db_parameter_group" "custom" {
  name   = "custom-pg-test-acme"
  family = "postgres9.5"
  parameter {
    name  = "timezone"
    value = "America/Sao_Paulo" apply_method = "pending-reboot"
  }
}

module "test2" {
  source  = "../"
  account = "${var.account}"

  name = "test-postgres2-acme"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  vpc_id = "${aws_vpc.default.id}"
  tags = {
    Foo = "bar"
  }
  password = "123mudar"
  username = "administrator"

  skip_final_snapshot = true
  multi_az = false

  custom_parameter_group_name = "${aws_db_parameter_group.custom.id}"
}

output "test2" {
  value = "${module.test2.db_instance}"
}
