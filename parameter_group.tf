// Parameter Group
resource "aws_db_parameter_group" "default" {
  name   = "${var.name}"
  family = "${var.family}"

  parameter { name = "log_autovacuum_min_duration"      value = "0" }
  parameter { name = "log_hostname"                     value = "0" }
  parameter { name = "log_lock_waits"                   value = "1" }
  parameter { name = "log_min_duration_statement"       value = "1000" }
  parameter { name = "log_temp_files"                   value = "0" }

  parameter { name = "track_io_timing"                  value = "1" }
  parameter { name = "track_functions"                  value = "all" }

//  parameter { name = "max_connections                   value = "TODO"              apply_method = "pending-reboot" }
  parameter { name = "work_mem"                         value = "4096" }
  parameter { name = "checkpoint_timeout"               value = "300" }

  parameter { name = "autovacuum_analyze_scale_factor"  value = "0.02" }
  parameter { name = "autovacuum_max_workers"           value = "3"                 apply_method = "pending-reboot" }
  parameter { name = "autovacuum_vacuum_scale_factor"   value = "0.05" }

  parameter { name = "timezone"                         value = "America/Sao_Paulo" }

  tags = "${var.tags}"
}

output "parameter_group" {
  value = {
    id  = "${aws_db_parameter_group.default.id}" 
    arn = "${aws_db_parameter_group.default.arn}"
  }
}
